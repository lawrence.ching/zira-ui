import _ from 'lodash'
import TodoStatus from './TodoStatus'

class Todo {
  constructor(id, title, status) {
    this.id = id;
    this.title = title;
    this.status = _.isNil(status) ? TodoStatus.PENDING : status
  }

  static generateNewId() {
    return 'zira-v0-' + Date.now();
  }
}

export default Todo;
