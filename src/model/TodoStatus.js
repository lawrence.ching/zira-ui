class TodoStatus {}

TodoStatus.PENDING = "pending";
TodoStatus.COMPLETED = "completed";

export default TodoStatus;

