// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Framework7 from 'framework7'
import Framework7Vue from 'framework7-vue'

Vue.config.productionTip = false;
Vue.use(Framework7Vue);

import Framework7Theme from 'framework7/dist/css/framework7.ios.min.css'

import store from './store';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: {App},
  template: '<App/>'
})
