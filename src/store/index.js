import _ from 'lodash'
import apis from '@/apis';
import Vue from 'vue'
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    todoList: []
  },
  mutations: {
    setTodoList(state, newList) {
      state.todoList = newList;
    },
    addTodoItem(state, newTodoItem) {
      state.todoList.push(newTodoItem);
    }
  },
  actions: {
    loadFromRemote({commit}) {
      return apis.loadAll().then((todoList) => {
        commit('setTodoList', todoList);
      })
    }
  }
});

function create(newTodo) {
  if (!_.isNil(newTodo.title)) {


  } else {
    return Promise.reject('title is required but not exist')
  }
}

export default store
