import config from '../config'
import _ from 'lodash';
import Todo from '@/model/Todo';

const URL = config.remoteUrl + '/todos';

function create(newTodo) {
  const body = JSON.stringify(_.omit(newTodo, ['id']));
  const url = URL + '/todo/' + newTodo.id;
  console.log('[PUT] ' + url);
  console.log('[PUT] ' + body);

  return fetch(url, {
    method: 'PUT',
    body: body,
    mode: 'cors',
    headers: new Headers({
      'Content-Type': 'application/json'
    })
  })
    .then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
      console.log('Success:', response);
      return response;
    });
}

function loadAll() {
  const url = URL + '/_search?q=*:*';
  return fetch(url, {
    mothod: 'GET',
  })
    .then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => {
      console.log('Success:', response);
      const array = response.hits.hits;
      return array.map((item) => {
        const _source = item._source;
        return new Todo(item._id, _source.title, _source.status);
      })
    });
}

export default {
  create, loadAll
}
