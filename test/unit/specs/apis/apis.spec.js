import apis from '@/apis';

describe('Apis', () => {

  describe('create()', () => {

    it('should reject if title is null', () => {
      return expect(
        apis.create({
          title: null
        })
      ).rejects.toMatch('title is required but not exist');
    });

    it('should reject if title is undefined', () => {
      return expect(
        apis.create({
          title: undefined
        })
      ).rejects.toMatch('title is required but not exist');
    });

    it('should resolve if title exists', () => {
      return apis.create({
        title: 'a valid title'
      });
    });


  });

  describe('loadAll()', () => {
      it('can load all todo items from remote server', (done) => {
        apis.loadAll().then((todoList) => {
          expect(todoList.length).toBe(1);

          const item = todoList[0];
          expect(item.title).toBe('Brand new Todo');
          expect(item.status).toBe('pending');
        });
      })
  })
});
